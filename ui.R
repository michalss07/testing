library(shiny)

ui <- fluidPage(
  sliderInput("slider1", "slider", 1, 100, 1),
  sliderInput("slider2", "slider", 1, 100, 1),
  sliderInput("slider3", "slider", 1, 100, 1),
  plotOutput("plot")
)

#shinyApp(ui = ui, server = server)